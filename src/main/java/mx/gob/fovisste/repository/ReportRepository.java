package mx.gob.fovisste.repository;

/**
 * @author Eder Sam
 *
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import mx.gob.fovisste.model.Report;

@Repository
public class ReportRepository {

	private static final String SQL_REPORT = "SELECT "
			+ "e.ID_ESTADO as stateId, "
			+ "e.NOMBRE as stateName, "
			+ "SUM(p.NUMERO_EMPLEADOS) as employeesNumber "
			+ "FROM test.dbo.ESTADOS e "
			+ "LEFT JOIN test.dbo.PLANTAS p ON e.ID_ESTADO = p.ID_ESTADO "
			+ "GROUP BY e.ID_ESTADO, e.NOMBRE "
			+ "ORDER BY 3 DESC";

	private static final BeanPropertyRowMapper<Report> ROW_MAPPER = new BeanPropertyRowMapper<>(Report.class);

	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	public Iterable<Report> generateReportSQL() {
		return jdbcTemplate.query(SQL_REPORT, ROW_MAPPER);
	}

}