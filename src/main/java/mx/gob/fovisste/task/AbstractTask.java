package mx.gob.fovisste.task;

import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * @author Eder Sam
 *
 */
public abstract class AbstractTask {

	public void runTask(String[] args) {
		new SpringApplicationBuilder().sources(getClass()).web(WebApplicationType.NONE).bannerMode(Banner.Mode.OFF)
				.run(args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ConfigurableApplicationContext ctx) {
		return args -> {
			try {
				executeTaskInternal(args);
			} catch (IllegalStateException e1) {
				// nothing
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				synchronized (ctx) {
					if (ctx.isActive()) {
						try {
							ctx.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		};
	}

	protected abstract void executeTaskInternal(String[] args) throws Exception;

}
