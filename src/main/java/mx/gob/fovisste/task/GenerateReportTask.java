package mx.gob.fovisste.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import mx.gob.fovisste.model.Report;
import mx.gob.fovisste.service.IReportService;

/**
 * @author Eder Sam
 *
 */
@SpringBootApplication(exclude = WebMvcAutoConfiguration.class)
@ComponentScan({ "mx.gob.fovisste" })
public class GenerateReportTask extends AbstractTask {

	private final static String PATH_REPORT = "src/main/resources/reports/report.txt";

	@Autowired
	private IReportService reportService;

	public static void main(String[] args) {
		GenerateReportTask generateReportTask = new GenerateReportTask();
		generateReportTask.runTask(args);
	}

	@Override
	protected void executeTaskInternal(String[] args) throws Exception {
		generateReport();
	}

	private void generateReport() throws FileNotFoundException, UnsupportedEncodingException {

		List<String> lines = new ArrayList<>();
		lines.add(line(new String[] { "ESTADO ID", "NÚMERO DE EMPLEADOS", "NOMBRE DE ESTADO" }));

		for (Report report : reportService.generateReport()) {
			Long employeesNumber = report.getEmployeesNumber() != null ? report.getEmployeesNumber() : 0L;
			lines.add(line(new String[] { report.getStateId(), employeesNumber.toString(), report.getStateName() }));
		}

		deleteFileIfExists();

		writeReport(lines);
	}

	private String line(String[] columns) {
		StringBuffer sb = new StringBuffer();
		sb.append(columns[0]);
		sb.append(",");
		sb.append(columns[1]);
		sb.append(",");
		sb.append(columns[2]);
		return sb.toString();
	}

	private void deleteFileIfExists() {
		File file = new File(PATH_REPORT);
		if (file.exists() && file.isFile()) {
			file.delete();
		}
	}

	private void writeReport(List<String> lines) throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter writer = new PrintWriter(PATH_REPORT, "UTF-8");
		for (String line : lines) {
			System.out.println(line);
			writer.println(line);
		}
		writer.close();
	}

}
