package mx.gob.fovisste.model;

/**
 * @author Eder Sam
 *
 */
public class Report {

	private String stateId;

	private Long employeesNumber;

	private String stateName;

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public Long getEmployeesNumber() {
		return employeesNumber;
	}

	public void setEmployeesNumber(Long employeesNumber) {
		this.employeesNumber = employeesNumber;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}