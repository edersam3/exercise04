package mx.gob.fovisste.service;

import mx.gob.fovisste.model.Report;

public interface IReportService {
    Iterable<Report> generateReport();
}