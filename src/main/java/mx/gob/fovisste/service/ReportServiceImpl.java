package mx.gob.fovisste.service;

/**
 * @author Eder Sam
 *
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.fovisste.model.Report;
import mx.gob.fovisste.repository.ReportRepository;

@Service
public class ReportServiceImpl implements IReportService {

	@Autowired
	private ReportRepository reportRepository;

	@Override
	public Iterable<Report> generateReport() {
		return reportRepository.generateReportSQL();
	}

}