4. Realice un programa en Java donde se conecta a SQL Server al servidor donde se
encuentran las siguientes tablas.

El programa debe crear un reporte con el total de empleados que existe en cada estado de la
republica donde en caso que no existan plantas en determinado estado este debe aparecer con
valor “Cero” adicional se debe organizar de mayor a menor en el número total de empleados en el
estado. El anterior reporte se deba guardar en un archivo de texto para crear este archivo se debe utilizar la clase StringBuffer y el reporte debe contener los siguientes campos en el encabezado del archivo de texto, ID_ESTADO, NUMERO_EMPLEADOS, NOMBRE del estado, para separar los campos utilice el campo “,” (coma).

	1. Para ejecutar el programa es necesario ejecutar la siguiente clase

			mx.gob.fovisste.task.GenerateReportTask
			
			NOTA: ejecutar como 'Sprin Boot App'

	2. El reporte generado se localizará en la siguiente ruta dentro del proyecto

			src/main/resources/reports/report.txt
			
	3. En eclipse es necesario refrescar la carpeta 'reports' para ver el reporte generado.